from django.contrib import admin
from django.conf.urls import include, url, patterns
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.models import User
from . import views

urlpatterns = patterns('',
    url('admin/', admin.site.urls),
    url('form-test/', include('formtest.urls')),
    url('wp-framework/', include('app_wpframework.urls')),
    url('media-root/', views.media_root)
)

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
