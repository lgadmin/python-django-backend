from main.settings.common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv("DEV_SECRET_KEY")

# DATABASE
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.getenv("DEV_DB_NAME"),
        'USER': os.getenv("DEV_DB_USER"),
        'PASSWORD': os.getenv("DEV_DB_PASSWORD"),
        'HOST': os.getenv("DEV_DB_HOST"),
        'PORT': os.getenv("DEV_DB_PORT"),
    }
}

ALLOWED_HOSTS = []

MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static")
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")
