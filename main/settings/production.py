from main.settings.common import *
import os

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv("PROD_SECRET_KEY")

SECURE_HSTS_SECONDS = 0
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = False
X_FRAME_OPTIONS = 'DENY'

# DATABASE
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.getenv("PROD_DB_NAME"),
        'USER': os.getenv("PROD_DB_USER"),
        'PASSWORD': os.getenv("PROD_DB_PASSWORD"),
        'HOST': os.getenv("PROD_DB_HOST"),
        'PORT': os.getenv("PROD_DB_PORT"),
    }
}

ALLOWED_HOSTS = ['form-test.longevitystaging.com', 'www.form-test.longevitystaging.com']

MEDIA_URL = '/site_media/'
STATIC_ROOT = os.path.join(os.path.dirname(os.path.dirname(BASE_DIR)), "static")
MEDIA_ROOT = os.path.join(os.path.dirname(os.path.dirname(BASE_DIR)), "site_media")