from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
from django.conf import settings

def media_root(request):

	return HttpResponse(settings.MEDIA_ROOT)