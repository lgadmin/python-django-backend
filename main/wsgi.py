"""
WSGI config for kelvintest project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""
import os
import sys
import re
import os.path

def read_env():
    """Pulled from Honcho code with minor updates, reads local default
    environment variables from a .env file located in the project root
    directory.
    """

    try:
        with open(os.path.dirname(__file__) + '/../.env') as f:
            content = f.read()
            print(content)
    except IOError,e:
        content = ''
        print(str(e))

    for line in content.splitlines():
        m1 = re.match(r'\A([A-Za-z_0-9]+)=(.*)\Z', line)
        if m1:
            key, val = m1.group(1), m1.group(2)
            m2 = re.match(r"\A'(.*)'\Z", val)
            if m2:
                val = m2.group(1)
            m3 = re.match(r'\A"(.*)"\Z', val)
            if m3:
                val = re.sub(r'\\(.)', r'\1', m3.group(1))
            os.environ.setdefault(key, val)

from django.core.wsgi import get_wsgi_application

read_env()

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main.settings.production")
application = get_wsgi_application()