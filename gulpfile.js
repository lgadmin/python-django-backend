// ----- gulp required var
var gulp                   = require('gulp'),
    watch                  = require('gulp-watch'),
    svgstore               = require('gulp-svgstore'),
    imagemin               = require('gulp-imagemin'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    imageminPngquant       = require('imagemin-pngquant'),
    jsmin                  = require('gulp-uglify'),
    sass                   = require('gulp-sass'),
    minifycss              = require('gulp-minify-css'),
    autoprefixer           = require('gulp-autoprefixer'),
    sourcemaps             = require('gulp-sourcemaps'),
    rename                 = require('gulp-rename'),
    buffer                 = require('vinyl-buffer'),
    ngAnnotate             = require('gulp-ng-annotate'),
    uglify                 = require('gulp-uglify'),
    source                 = require('vinyl-source-stream'),
    browserify             = require('browserify'),
    argv                   = require('minimist')(process.argv);

var appRoot = argv.app + '/';
var assetsRoot = argv.app + '/assets/';
var distRoot = argv.app + '/static/' + argv.app + '/';
var nodePath = './node_modules';

var sassLoad = [
    nodePath + '/'
];

var src = {
  image: assetsRoot + 'images',
  sass: assetsRoot + 'sass',
  js: assetsRoot + 'js',
};

var dist = {
  image: distRoot + 'images',
  css: distRoot + 'css',
  js: distRoot + 'js',
};

// ----- Image Min
gulp.task("image", function(){
    return gulp.src(src.image + '/**/*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imageminPngquant({quality: '75-85'}),
            imagemin.svgo({plugins: [{removeViewBox: false}]})
        ]))
        .pipe(gulp.dest(dist.image));
});

// ----- SASS
gulp.task('css-main', function() {
    return gulp.src(src.sass + '/**/style.scss')
        .pipe(sass({ 
          outputStyle: 'compressed',
          includePaths: sassLoad,
          onError: function(err) { return notify().write(err); }
        }).on('error', sass.logError))
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifycss())
        .pipe(gulp.dest(dist.css)); 
});

// ------ Vendor CSS
gulp.task('css-vendor', function() {
    return gulp.src([
      nodePath + '/selectric/public/selectric.css'
    ])
     .pipe(sourcemaps.init())
     .pipe( sass({ outputStyle: 'compressed',})
     .on('error', sass.logError))
     .pipe(autoprefixer({ browsers: ['last 3 versions']}))
     .pipe(sourcemaps.write('.'))
     .pipe(concat('vendor.min.css'))
     .pipe(gulp.dest(gulp.dest(dist.css)))
});

// ------ appJS
gulp.task('js-main', function() {
    return browserify(src.js + '/app.js')
        .bundle()
        .pipe(source('app.min.js'))
        .pipe(buffer())
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest(dist.js));
});

// ----- Watch file change
gulp.task('watch', function() {

  gulp.watch(src.sass + '/**/*.scss', ['css-main']);
  gulp.watch(src.js + '/**/*.js', ['js-main']);
  gulp.watch(src.image + '/**/*', ['image']);

});

// Default task
gulp.task('default', ['image', 'css-main', 'js-main', 'watch']);
// Build task
gulp.task('build', ['image', 'css-main', 'js-main']);