import os, formtest, time, datetime, json, imaplib, email, pytz, requests
from datetime import timedelta
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Website
from .serializer import WebsiteSerializer
from django.contrib.auth.decorators import user_passes_test

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import timedelta

@user_passes_test(lambda u: u.is_superuser)
def report(request):
	return render(request, 'formtest/report.html')

# Generate Form Test report from the database
def pull_report(request):
	websites = Website.objects.all()
	serializer = WebsiteSerializer(websites, many=True)
	time = datetime.datetime.now()

	data = {"server_time": time, "data": serializer.data};

	return JsonResponse(data, safe=False)

# Send trigger to restful endpoints on the wordpress websites to send the submission to testing email account (lgformtest@gmail.com)
def notify(request):
	websites = Website.objects.all()
	response = '';
	index = 1;

	for website in websites:
		try:
		    content = requests.get("http://" + website.url + "/wp-json/lg_form_testing/v1/run")
		except Exception:
			response += index + ': ' + website.url + '\n'

	if response == '':
		response = 'success'

	return HttpResponse(response)

# Same with notify function, but send trigger for the website thats selected
def validate(request):
	if request.method == 'POST':
		body_unicode = request.body.decode('utf-8')
		body = json.loads(body_unicode)
		url = "http://" + body['url'] + "/wp-json/lg_form_testing/v1/run"

		try:
		    content = requests.get(url)
		    return HttpResponse(content)

		except Exception:
		    time.sleep(1)
		    return HttpResponse('failed')

# Database Reset - set last succesful check time to default for all the website entries.
def reset(request):
	websites = Website.objects.all()

	for website in websites:
		website.last_successful_check = datetime.date(2000, 1, 1)
		website.save()

	return HttpResponse('success');

# Fetch testing gmail account (lgformtest@gmail.com) to get all the unread emails and update last successful check entry for websites that found in the email box.
def fetch(request):
	mail = imaplib.IMAP4_SSL('imap.gmail.com')
	mail.login('lgformtest@gmail.com', 'longevity')
	mail.list()
	# Out: list of "folders" aka labels in gmail.
	mail.select("inbox") # connect to inbox.

	result, data = mail.search(None, "(UNSEEN)")
 
	ids = data[0] # data is a list.
	id_list = ids.split() # ids is a space separated string

	not_found = []
	message = 'Domains not found:'

	if id_list:
		for email_id in id_list:
			result, data = mail.fetch(email_id, "(RFC822)")
			email_message = email.message_from_string(data[0][1])
			subject = email_message['subject']
			
			target = Website.objects.filter(url=subject)
			date_tuple = email.utils.parsedate(email_message['Date'])
			if date_tuple:
			    local_date = pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(date_tuple)))
			    if target:
			    	target = target[0]
			    	if target.last_successful_check < local_date:
			    		target.last_successful_check = local_date
			    		target.save()
			    else:
					not_found.append(subject)

	# Notification email - Send to Longevity Graphics Stuff a list of unsuccessfully checked website url.
	websites = Website.objects.all()
	unchecked_list = [];
	now = pytz.utc.localize(datetime.datetime.utcnow())

	email_from = "lgformtest@gmail.com"
	email_to = "admin@longevitygraphics.com"

	for website in websites:
		if website.last_successful_check + timedelta(days=1) < now:
			unchecked_list.append(website.url)

	s = smtplib.SMTP("smtp.gmail.com", 587)
	s.ehlo()
	s.starttls()
	s.login(email_from, "longevity")

	msg = MIMEMultipart('alternative')
	msg['Subject'] = "LG FORM TEST"
	msg['From'] = email_from
	msg['To'] = email_to

	uncheck_item_list = "<div style='padding: 10px 0;'>"
	for item in unchecked_list:
		uncheck_item_list = uncheck_item_list + "<div>" + item + "</div>"
	uncheck_item_list = uncheck_item_list + "</div>"

	email_html = "<h2>FORM TESTING</h2>" + "<p>Below website didn't pass validation</p>" + uncheck_item_list

	msg.attach(MIMEText(email_html, 'html'))
	s.sendmail(email_from, email_to, msg.as_string())

	# Return Message
	for item in not_found:
		message = message + '<br>' + item

	return HttpResponse(message);