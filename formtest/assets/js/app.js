require('angular');

//ngApp config
var app = angular.module('app', [])
	.config(function($httpProvider, $interpolateProvider){
		$httpProvider.defaults.xsrfCookieName = 'csrftoken';
	    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
	    //** django urls loves trailling slashes which angularjs removes by default.
	    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	})

//Controllers
var appCtrl = require('./controller/appCtrl');
app.controller('appCtrl', ['$scope', '$http', '$interval', appCtrl]);