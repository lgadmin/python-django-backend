module.exports = function($scope, $http, $interval) {
    //Setting variables
	var pull_report_url = '/form-test/pull_report';
    var validate_now_url = '/form-test/validate/';
    var notify_all_url = '/form-test/notify/';
    var validate_all_url = '/form-test/fetch/';
    var reset_all_url = '/form-test/reset/';

    //Scope variables
	$scope.dataList = [];
    $scope.order = 'website';
    $scope.reverse = true;
    $scope.filter = 'all';
    $scope.server_time = '';
    $scope.loading = false;

    //First data load from server
    load();

    /**
     * Load current threat data from server with http request
     * @param none
     * @return none
     */
    function load(){
        $scope.loading = true;

        $http.get(pull_report_url).then(function(result) {
            $scope.dataList = result.data.data;
            $scope.server_time = Date.parse(result.data.server_time);

            $scope.dataList.map(function(item){
                item.last_successful_check = new Date(item.last_successful_check).toLocaleString()
                item.date_created = new Date(item.date_created).toLocaleString()
            });
            $scope.loading = false;
        }, function(){
            console.log('error');
        });
    }

    /**
     * Update time filter from front-end selection
     * @param String filterValue
     * @return none
     */
    function updateFilter(filterValue){
        $scope.filter = filterValue;
    }

    /**
     * Compare dataDate with currentDate. Helper function for date filter functionality 
     * @param String dataDate
     * @param daysFilter
     * @return Boolean
     */
    function dataFilter(data){
        switch ($scope.filter) {
            case 'all':
                return true;
                break;
            case 'successful':
                return isChecked(data);
                break;
            case 'unsuccessful':
                return !isChecked(data);
                break;
        }
    }

    function validateNow(url){
        $scope.loading = true;

        $http.post(validate_now_url, { 'url' : url }).then(function(result) {
            $scope.loading = false;
        }, function(){
            console.log('error');
        });
    }

    /**
    */
    function isChecked(data){
        var data_time = Date.parse(data.last_successful_check)
        var twentyfour_hours = 1000 * 60 * 60 * 24;

        if($scope.server_time - data_time > twentyfour_hours){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Sort function helper.
     * @param String filtername
     * @return none
     */
    function sortBy(orderName) {
        $scope.reverse = ($scope.order === orderName) ? !$scope.reverse : false;
        $scope.order = orderName;
    };

    /*
     *
    */
    function notifyAll(){
        $scope.loading = true;

        $http.post(notify_all_url, {}).then(function(result) {
            $scope.loading = false;
        }, function(){
            console.log('error');
        });
    }

    /*
     *
    */
    function validateAll(){
        $scope.loading = true;

        $http.post(validate_all_url, {}).then(function(result) {
            load();
            $scope.loading = false;
        }, function(){
            console.log('error');
        });
    }

    /*
     *
    */
    function resetAll(){
        $scope.loading = true;
        $http.post(reset_all_url, {}).then(function(result) {
            load();
            $scope.loading = false;
        }, function(){
            console.log('error');
        });
    }

    //Scope functions
    $scope.load = load;
    $scope.sortBy = sortBy;
    $scope.dataFilter = dataFilter;
    $scope.isChecked = isChecked;
    $scope.validateNow = validateNow;
    $scope.notifyAll = notifyAll;
    $scope.validateAll = validateAll;
    $scope.resetAll = resetAll;
};