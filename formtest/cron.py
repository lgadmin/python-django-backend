import imaplib, email, time, datetime, kronos, pytz, smtplib, urllib2
from .models import Website
from django.http import HttpResponse
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import timedelta

@kronos.register('0 0 * * *')
def notify():
	websites = Website.objects.all()

	for website in websites:
		try:
		    content = urllib2.urlopen("http://" + website.url + "/wp-json/lg_form_testing/v1/run").read()
		except urllib2.HTTPError, e:
		    print(e.code)
		except urllib2.URLError, e:
		    print(e.args)

@kronos.register('0 12 * * *')
def fetch():
	mail = imaplib.IMAP4_SSL('imap.gmail.com')
	mail.login('lgformtest@gmail.com', 'longevity')
	mail.list()
	# Out: list of "folders" aka labels in gmail.
	mail.select("inbox") # connect to inbox.

	result, data = mail.search(None, "(UNSEEN)")
 
	ids = data[0] # data is a list.
	id_list = ids.split() # ids is a space separated string

	if id_list:
		for email_id in id_list:
			result, data = mail.fetch(email_id, "(RFC822)")
			email_message = email.message_from_string(data[0][1])
			subject = email_message['subject']
			print(subject)
			try:
			    target = Website.objects.filter(url=subject)
			except SomeModel.DoesNotExist:
			    target = None
			date_tuple = email.utils.parsedate(email_message['Date'])
			print(date_tuple)
			if date_tuple:
			    print(target)
			    local_date = pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(date_tuple)))
			    if target != None:
			    	target = target[0]
			    	if target.last_successful_check < local_date:
			    		target.last_successful_check = local_date
			    		target.save()
			else:
			    print('what the hell?')
	# Notification email
	websites = Website.objects.all()
	unchecked_list = [];
	now = pytz.utc.localize(datetime.datetime.utcnow())

	email_from = "lgformtest@gmail.com"
	email_to = "admin@longevitygraphics.com"

	for website in websites:
		if website.last_successful_check + timedelta(days=1) < now:
			unchecked_list.append(website.url)

	s = smtplib.SMTP("smtp.gmail.com", 587)
	s.ehlo()
	s.starttls()
	s.login(email_from, "longevity")

	msg = MIMEMultipart('alternative')
	msg['Subject'] = "LG FORM TEST"
	msg['From'] = email_from
	msg['To'] = email_to

	uncheck_item_list = "<div style='padding: 10px 0;'>"
	for item in unchecked_list:
		uncheck_item_list = uncheck_item_list + "<div>" + item + "</div>"
	uncheck_item_list = uncheck_item_list + "</div>"

	email_html = "<h2>FORM TESTING</h2>" + "<p>Below website didn't pass validation</p>" + uncheck_item_list

	msg.attach(MIMEText(email_html, 'html'))
	s.sendmail(email_from, email_to, msg.as_string())
