from django.db import models
from datetime import datetime


class Website(models.Model):
	url = models.CharField(max_length=255)
	last_successful_check = models.DateTimeField('last successful check', default=datetime.now)
	notes = models.TextField(max_length=255)
	date_created = models.DateTimeField('date created', default=datetime.now)
	def __str__(self):
    		return self.url