# encoding=utf8

import json

# Validate if file is json farmat
def isJson(file):
  try:
    json_object = json.loads(file)
  except ValueError, e:
    return False
  return True

# format string to follow json standards
def formatJsonString(jsonString):
	return jsonString.replace("'", "\"").replace("‘", "\"" ).replace("’", "\"")