# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('formtest', '0002_auto_20180418_2139'),
    ]

    operations = [
        migrations.AddField(
            model_name='website',
            name='image',
            field=models.ImageField(default=1, upload_to=b'form_test'),
            preserve_default=False,
        ),
    ]
