# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('formtest', '0003_website_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='website',
            name='image',
        ),
    ]
