# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('formtest', '0005_website_notes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='website',
            name='notes',
            field=models.TextField(max_length=255),
        ),
    ]
