# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('formtest', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Website',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=255)),
                ('last_successful_check', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'last successful check')),
                ('date_created', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'date created')),
            ],
        ),
        migrations.DeleteModel(
            name='Threat',
        ),
        migrations.DeleteModel(
            name='ThreatUpdateDate',
        ),
    ]
