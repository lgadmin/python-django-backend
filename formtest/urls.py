from django.contrib import admin
from django.conf.urls import url, patterns
from . import views

urlpatterns = patterns('',
    url(r'^$', views.report),
    url(r'^pull_report/$', views.pull_report),
    url(r'^notify/$', views.notify),
    url(r'^fetch/$', views.fetch),
    url(r'^reset/$', views.reset),
    url(r'^validate/$', views.validate)
)
