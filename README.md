# README #

Contributor: Kelvin Xu

### What is this repository for? ###

From test service backend for Longevity Graphics

### Version ###

Python 2.7
Django 1.8

### Local ###

* pip install -r requirements.txt
* npm install
* gulp build --app {appname}   in this application, use 'gulp build --app formtest'

* export DJANGO_SETTINGS_MODULE="main.settings.development"

* python manage.py collectstatic
* python manage.py installtasks
* python manage.py migrate
* python manage.py runserver
* Project should start running at http://localhost:8000/formtest/



### Deploy ###

* pip install -r requirements.txt
* npm install

* gulp build --app {appname}   in this application, use 'gulp build --app formtest'
* (On Cartika Server, use node_modules/gulp/bin/gulp.js build --app formtest)

* export DJANGO_SETTINGS_MODULE="main.settings.production"

* python manage.py collectstatic
* python manage.py installtasks
* python manage.py migrate
* python manage.py runserver