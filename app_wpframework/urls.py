from django.contrib import admin
from django.conf.urls import url, patterns, include
from . import views
from .models import Plugin, Theme, PluginCategory, ThemeCategory
from .serializers import PluginSerializer, ThemeSerializer, PluginCategorySerializer, ThemeCategorySerializer
from rest_framework import routers, serializers, viewsets

class PluginViewSet(viewsets.ModelViewSet):
    queryset = Plugin.objects.all()
    serializer_class = PluginSerializer

class PluginCategoryViewSet(viewsets.ModelViewSet):
    queryset = PluginCategory.objects.all()
    serializer_class = PluginCategorySerializer

class ThemeViewSet(viewsets.ModelViewSet):
    queryset = Theme.objects.all()
    serializer_class = ThemeSerializer

class ThemeCategoryViewSet(viewsets.ModelViewSet):
    queryset = ThemeCategory.objects.all()
    serializer_class = ThemeCategorySerializer

router = routers.DefaultRouter()
router.register(r'plugins', PluginViewSet)
router.register(r'plugins_category', PluginCategoryViewSet)
router.register(r'themes', ThemeViewSet)
router.register(r'themes_category', ThemeCategoryViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls'))
)
