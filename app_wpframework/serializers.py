from rest_framework import serializers
from .models import Plugin, Theme, ThemeCategory, PluginCategory

class PluginSerializer(serializers.ModelSerializer):

    class Meta:
        model = Plugin
        fields = ('id', 'name', 'image', 'zipUrl', 'folderName', 'pluginFile', 'category')

    category = serializers.SerializerMethodField('get_category_slug')

    def get_category_slug(self, obj):
        return obj.category.slug

class PluginCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PluginCategory
        fields = ('name', 'slug')
        def __unicode__(self):
            return '%d: %s' % (self.name, self.slug)

class ThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Theme
        fields = ('id', 'name', 'image', 'zipUrl', 'category')

    category = serializers.SerializerMethodField('get_category_slug')

    def get_category_slug(self, obj):
        return obj.category.slug

class ThemeCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ThemeCategory
        fields = ('name', 'slug')