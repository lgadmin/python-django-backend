# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0010_remove_plugin_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='plugin',
            name='image',
            field=models.ImageField(default=b'project-1.jpg', upload_to=b'app_wpframework'),
        ),
    ]
