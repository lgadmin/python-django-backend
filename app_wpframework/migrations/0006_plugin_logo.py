# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0005_remove_plugin_logo'),
    ]

    operations = [
        migrations.AddField(
            model_name='plugin',
            name='logo',
            field=models.ImageField(default=datetime.datetime(2018, 10, 18, 18, 55, 55, 45071, tzinfo=utc), upload_to=b'app_wpframework'),
            preserve_default=False,
        ),
    ]
