# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0020_theme'),
    ]

    operations = [
        migrations.CreateModel(
            name='ThemeCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('slug', models.SlugField()),
            ],
            options={
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.AlterField(
            model_name='theme',
            name='category',
            field=models.ForeignKey(blank=True, to='app_wpframework.ThemeCategory', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='themecategory',
            unique_together=set([('name', 'slug')]),
        ),
    ]
