# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0013_remove_plugin_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='plugin',
            name='image',
            field=models.ImageField(null=True, upload_to=b'app_wpframework', blank=True),
        ),
    ]
