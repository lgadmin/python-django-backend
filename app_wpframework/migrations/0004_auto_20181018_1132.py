# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0003_auto_20181018_1103'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plugin',
            name='logo',
            field=models.ImageField(upload_to=b''),
        ),
    ]
