# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0019_auto_20181024_1528'),
    ]

    operations = [
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(default=0, editable=False, db_index=True)),
                ('name', models.CharField(max_length=255)),
                ('image', models.CharField(max_length=255)),
                ('zipUrl', models.CharField(max_length=255)),
                ('category', models.CharField(max_length=255, choices=[(b'bronze', b'Bronze'), (b'silver', b'Silver'), (b'gold', b'Gold'), (b'other', b'Other')])),
            ],
            options={
                'ordering': ['order'],
                'abstract': False,
            },
        ),
    ]
