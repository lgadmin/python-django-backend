# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0012_auto_20181022_1123'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plugin',
            name='image',
        ),
    ]
