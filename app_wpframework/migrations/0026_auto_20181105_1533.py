# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0025_auto_20181105_1532'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plugin',
            name='category',
            field=models.ForeignKey(blank=True, to='app_wpframework.PluginCategory', null=True),
        ),
    ]
