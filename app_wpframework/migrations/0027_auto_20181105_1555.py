# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0026_auto_20181105_1533'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plugin',
            name='category',
            field=models.ForeignKey(related_name='category', blank=True, to='app_wpframework.PluginCategory', null=True),
        ),
    ]
