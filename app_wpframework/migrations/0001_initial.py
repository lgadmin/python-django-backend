# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Plugin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('zipUrl', models.CharField(max_length=255)),
                ('folderName', models.CharField(max_length=255)),
                ('PluginFile', models.CharField(max_length=255)),
            ],
        ),
    ]
