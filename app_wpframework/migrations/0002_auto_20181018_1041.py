# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='plugin',
            name='logo',
            field=models.ImageField(default=b'app_wpframework/static/app_wpframework/400x400.png', upload_to=b''),
        ),
        migrations.AddField(
            model_name='plugin',
            name='name',
            field=models.CharField(default=b'new plugin', max_length=255),
        ),
    ]
