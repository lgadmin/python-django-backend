# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0023_themecategory_order'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='themecategory',
            options={'ordering': ['order'], 'verbose_name_plural': 'Themes Categories'},
        ),
    ]
