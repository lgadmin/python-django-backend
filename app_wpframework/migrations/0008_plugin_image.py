# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0007_auto_20181018_1330'),
    ]

    operations = [
        migrations.AddField(
            model_name='plugin',
            name='image',
            field=models.ImageField(default=1, upload_to=b'app_wpframework'),
            preserve_default=False,
        ),
    ]
