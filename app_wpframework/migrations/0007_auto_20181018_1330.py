# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0006_plugin_logo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plugin',
            name='logo',
        ),
        migrations.AlterField(
            model_name='plugin',
            name='name',
            field=models.CharField(max_length=255),
        ),
    ]
