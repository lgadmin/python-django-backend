# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0024_auto_20181105_1529'),
    ]

    operations = [
        migrations.CreateModel(
            name='PluginCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(default=0, editable=False, db_index=True)),
                ('name', models.CharField(max_length=200)),
                ('slug', models.SlugField()),
            ],
            options={
                'ordering': ['order'],
                'verbose_name_plural': 'Plugins Categories',
            },
        ),
        migrations.AlterUniqueTogether(
            name='plugincategory',
            unique_together=set([('name', 'slug')]),
        ),
    ]
