# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0016_auto_20181024_1200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plugin',
            name='category',
            field=models.CharField(max_length=255, choices=[(b'mu', b'Must Use'), (b'r', b'Recommend'), (b'o', b'Optional')]),
        ),
    ]
