# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0021_auto_20181105_1520'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='themecategory',
            options={'verbose_name_plural': 'Themes Categories'},
        ),
    ]
