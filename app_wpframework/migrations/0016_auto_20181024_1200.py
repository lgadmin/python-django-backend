# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0015_remove_plugin_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='plugin',
            old_name='PluginFile',
            new_name='image',
        ),
        migrations.AddField(
            model_name='plugin',
            name='category',
            field=models.CharField(default=1, max_length=1, choices=[(b'mu', b'Must Use'), (b'r', b'Recommend'), (b'o', b'Optional')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='plugin',
            name='pluginFile',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
    ]
