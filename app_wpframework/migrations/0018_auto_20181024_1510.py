# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_wpframework', '0017_auto_20181024_1203'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='plugin',
            options={'ordering': ['order']},
        ),
        migrations.AddField(
            model_name='plugin',
            name='order',
            field=models.PositiveIntegerField(default=0, editable=False, db_index=True),
        ),
    ]
