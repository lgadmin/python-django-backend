from django.db import models
from adminsortable.models import Sortable

class Plugin(Sortable):

	name = models.CharField(max_length=255)
	image = models.CharField(max_length=255)
	zipUrl = models.CharField(max_length=255)
	folderName = models.CharField(max_length=255)
	pluginFile = models.CharField(max_length=255)
	category = models.ForeignKey('PluginCategory', null=True, blank=True)
	class Meta:
		ordering = ['order']
	def __str__(self):
    		return self.name

class PluginCategory(Sortable):
    name = models.CharField(max_length=200)
    slug = models.SlugField()

    class Meta:
        unique_together = ('name', 'slug',)
        verbose_name_plural = "Plugins Categories"
        ordering = ['order']

    def __str__(self):
        return self.name

class Theme(Sortable):

	name = models.CharField(max_length=255)
	image = models.CharField(max_length=255)
	zipUrl = models.CharField(max_length=255)
	category = models.ForeignKey('ThemeCategory', null=True, blank=True)
	def __str__(self):
    		return self.name
	class Meta:
		ordering = ['order']


class ThemeCategory(Sortable):
    name = models.CharField(max_length=200)
    slug = models.SlugField()

    class Meta:
        unique_together = ('name', 'slug',)
        verbose_name_plural = "Themes Categories"
        ordering = ['order']

    def __str__(self):
        return self.name