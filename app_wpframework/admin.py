from django.contrib import admin
from .models import Plugin, Theme, ThemeCategory, PluginCategory
from adminsortable.admin import SortableAdmin

class PluginAdmin(SortableAdmin):
    pass
admin.site.register(Plugin, PluginAdmin)

class PluginCategoryAdmin(SortableAdmin):
    pass
admin.site.register(PluginCategory, PluginCategoryAdmin)

class ThemeAdmin(SortableAdmin):
    pass
admin.site.register(Theme, ThemeAdmin)

class ThemeCategoryAdmin(SortableAdmin):
    pass
admin.site.register(ThemeCategory, ThemeCategoryAdmin)