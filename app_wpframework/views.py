from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Plugin
from .serializers import PluginSerializer
import json