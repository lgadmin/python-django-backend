from django.apps import AppConfig


class WpFrameworkConfig(AppConfig):
    name = 'app_wpframework'
